import 'package:flutter/material.dart';
import 'package:intro_to_flutter/main.dart';
//todo - testar alt+enter (eliminar material.dart)                                (!) 1

class TesteDeIDE extends StatelessWidget {
  const TesteDeIDE({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (mensagem.isEmpty) {
      //todo ir para declaração usando atalho                                     (!) 3
      mensagem = "olá";
    } else {
      //todo mover bloco para cá usando o atalho                                  (!) 4
    }
    if (mensagem == "olá") {
      mensagem = "tchau";
    } else {
      mensagem = "olá";
    }

    return Container(
      //todo - testar ctrl + espaço (mudar cor)                                   (!) 2
      color: Colors.green,
      child: Text(mensagem),
    );
  }

  //todo mudar o nome da variavel mensagem (mostrar alternativa e multilinha)     (!) 6

  //todo substituir pelo nome do usuário usando edição multilinha                 (!) 5
  String mensagemDeBoasVindas() {
    return "Seja bem-vindo USER";
  }

  String mensagemDeDespedida() {
    return "Até mais NOME";
  }

  //todo colocar hifen na frente dos itens                                        (!) 7
  String listaDeTarefas() {
    return '''
    item 1
    item 2
    item 3
    item 4
    item 5
    item 6
    ''';
  }
}
