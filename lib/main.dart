import 'package:flutter/material.dart';

String mensagem = "Olá";

void main() {
  runApp(MyApp());
}

//Treinar em
// https://dartpad.dev/?null_safety=true

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'IDE e Sintaxe',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          children: [
            RaisedButton(
              color: Colors.green,
              onPressed: () {
                setState(() {});
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.check_circle_outline),
                  SizedBox(
                    width: 10,
                  ),
                  Text('Press me'),
                ],
              ),
            ),
            TestButton()
          ],
        ),
      ),
    );
  }
}

class TestButton extends StatefulWidget {
  const TestButton({Key? key}) : super(key: key);

  @override
  _TestButtonState createState() => _TestButtonState();
}

class _TestButtonState extends State<TestButton> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.green,
      onPressed: () {
        counter += 1;
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check_circle_outline),
          SizedBox(
            width: 10,
          ),
          Text('$counter'),
        ],
      ),
    );
  }
}
