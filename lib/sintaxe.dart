import 'package:flutter/cupertino.dart';

class ExemploSintaxe {
  static String variavelEstatica = "s";
}

class WidgetSintaxe extends StatelessWidget {
  const WidgetSintaxe({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //                                                         (!) static não precisa instanciar para acessar
    final String tmp = ExemploSintaxe.variavelEstatica;
    //                                                         (!) tmp será igual a partir da atribuição
    //                                                         (!) tmpConst sempre será igual a "c"
    const String tmpConst = "c";

    /*
                                                                 - Tipos de variaveis
     */
    int ex1 = 1 + 1;
    double ex2 = 1.1 + 1.1;
    List<int> lista = [0, 1, 2];
    Map<String, int> mapa = {
      "um": 1,
      "dois": 2,
      "três": 3,
    };
    dynamic dinamico = mapa;
    dinamico = lista;

    //                                                         atribuição condicional
    ex2 = ex2 > 2 ? 3 : 1;

    //                                                         Nulo ou não nulo
    String? variavelNula;
    String variavelNaoNula = variavelNula ?? "";

    return Container();
  }
}
